# M.A.L.S. Intermodule Broker (MIB)

![Version](https://img.shields.io/badge/dynamic/json?color=informational&label=version&query=%24.general.version&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fmals-intermodule-broker%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![License](https://img.shields.io/badge/dynamic/json?color=green&label=license&query=%24.general.license&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fmals-intermodule-broker%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/mals-intermodule-broker/src/branch/main/LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fmals-intermodule-broker%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![JDK version](https://img.shields.io/badge/dynamic/json?color=informational&label=JDK&query=%24.specific.jdk&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fmals-intermodule-broker%2Fraw%2Fbranch%2Fmain%2Fshields.json)

## Description

A simple library for transferring messages between program modules. It uses the publisher-subscriber principle and serves as a broker (similar to the MQTT protocol).

### Scope

The library targets applications that consist of multiple autonomous and equivalent modules that need to communicate with each other.

An example is a robotic system where each activity is handled by a separate module (motor control, perception, collision avoidance, ...). These modules need to communicate with each other, but from the perspective of the sending module it may not always be clear which modules will use a given message. It is therefore advantageous for the modules interested in the messages to handle the reception themselves, through subscriptions to their topics.

### Working principle

Application modules send messages to the broker. Each message has a topic, which the broker uses to decide to whom to deliver the sent message. The decision is based on the subscriptions that the modules set to the topics.

If a message has a topic that no one subscribes to, it is discarded.

## Current state

The library is currently in a functional state (except the `unsubscribe` functionality), but is still waiting to be tested in a real deployment.

## Using

In progress.

## Dependencies

- SLF4J \[1.7.36\] - framework independent logging
- JUnit \[4.13.x\] - unit testing
- Mockito \[4.9.x\] - mocking for unit testing

## Contributing

Contributions are welcome!

## License

The library is licensed under the [LGPL v2.1](https://codeberg.org/atlas144/mals-intermodule-broker/src/branch/main/LICENSE).
