// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.conversion;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class PayloadConvertorTest {
    
    @Test
    public void testEncodePayload() {
        short[] payload = {(short) 0, (short) 1, (short) 2};
        
        String expResult = "0|1|2";
        String result = PayloadConvertor.encodePayload(payload);
        
        assertEquals(expResult, result);
    }

    @Test
    public void testDecodePayload() {
        String payload = "0|1|2";
        
        short[] expResult = {(short) 0, (short) 1, (short) 2};
        short[] result = PayloadConvertor.decodePayload(payload);
        
        assertArrayEquals(expResult, result);
    }
    
}
