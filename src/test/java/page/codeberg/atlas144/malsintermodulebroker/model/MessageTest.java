// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.model;

import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class MessageTest {
    
    @Test
    public void testCompareToGreater() {
        Message greaterPriorityMessage = new Message("", new JSONObject(), Priority.IMPORTANT);
        Message testedMessage = new Message("", new JSONObject(), Priority.NORMAL);
    
        int expResult = testedMessage.compareTo(greaterPriorityMessage);
        int result = -1;
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCompareToSame() {
        Message samePriorityMessage = new Message("", new JSONObject(), Priority.NORMAL);
        Message testedMessage = new Message("", new JSONObject(), Priority.NORMAL);
    
        int expResult = testedMessage.compareTo(samePriorityMessage);
        int result = 0;
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCompareToSmaller() {
        Message smallerPriorityMessage = new Message("", new JSONObject(), Priority.UNIMPORTANT);
        Message testedMessage = new Message("", new JSONObject(), Priority.NORMAL);
    
        int expResult = testedMessage.compareTo(smallerPriorityMessage);
        int result = 1;
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTopic() {
        String testTopic = "testTopic";
        Message testedMessage = new Message(testTopic, new JSONObject(), Priority.NORMAL);
        
        String expResult = testedMessage.topic();
        String result = testTopic;
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPayload() {
        JSONObject testPayload = new JSONObject();
        Message testedMessage = new Message("", testPayload, Priority.NORMAL);
        
        testPayload.put("value", "payload");
        
        JSONObject expResult = testedMessage.payload();
        JSONObject result = testPayload;
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPriority() {
        Priority testPriority = Priority.NORMAL;
        Message testedMessage = new Message("", new JSONObject(), Priority.NORMAL);
        
        Priority expResult = testedMessage.priority();
        Priority result = testPriority;
        
        assertEquals(expResult, result);
    }
    
}
