// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.intercommunication;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.subscriptions.SubscriptionsTrie;
import page.codeberg.atlas144.malsintermodulebroker.exceptions.UnregisteredTopicException;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
@RunWith(MockitoJUnitRunner.class)
public class IntercommunicationModuleTest {
    
    private IntercommunicationModule intercommunicationModule;
    private BlockingQueue<Message> messageQueue;
    @Mock
    private SubscriptionsTrie<TaskModule> subscriptions;
    @Mock
    private TaskModule subscriber;
    
    @Before
    public void setup() {
        messageQueue = new PriorityBlockingQueue<>();
        
        intercommunicationModule = new IntercommunicationModule(messageQueue, subscriptions);
    }
    
    @Test
    public void testPublish() {
        String topic = "/test";
        JSONObject payload = new JSONObject();
        Priority priority = Priority.NORMAL;

        payload.put("value", "payload");
        intercommunicationModule.publish(topic, payload, priority);
        
        Message message = messageQueue.peek();
        
        assertEquals(messageQueue.size(), 1);
        assertEquals(message.topic(), topic);
        assertEquals(message.payload(), payload);
        assertEquals(message.priority(), priority);
    }
    
    @Test
    public void testSubscribe() {
        String topic = "/test";
        
        intercommunicationModule.subscribe(topic, subscriber);
        
        verify(subscriptions).subscribe(topic, subscriber);
    }
    
    @Test
    public void testUnsubscribe() throws UnregisteredTopicException {
        String topic = "/test";
        
        intercommunicationModule.subscribe(topic, subscriber);
        intercommunicationModule.unsubscribe(topic, subscriber);
        
        verify(subscriptions).unsubscribe(topic, subscriber);
    }
    
    @Test
    public void testUnsubscribeException() throws UnregisteredTopicException {
        String topic = "/test";
        
        doThrow(UnregisteredTopicException.class).when(subscriptions).unsubscribe(topic, subscriber);
        
        intercommunicationModule.unsubscribe(topic, subscriber);
        
        verify(subscriptions).unsubscribe(topic, subscriber);
    }
    
    @Test
    public void testRun() throws InterruptedException {
        String topic = "/test";
        JSONObject payload = new JSONObject();
        Priority priority = Priority.NORMAL;
        Message message = new Message(topic, payload, priority);
        
        payload.put("value", "payload");
        messageQueue.add(message);
        
        intercommunicationModule.start();
        Thread.sleep(1000);
        intercommunicationModule.interrupt();
        
        assertTrue(messageQueue.isEmpty());
    }
    
    @Test
    public void testRunWithSubscribers() throws InterruptedException, UnregisteredTopicException {
        String topic = "/test";
        JSONObject payload = new JSONObject();
        Priority priority = Priority.NORMAL;
        Message message = new Message(topic, payload, priority);
        Set subscribers = new LinkedHashSet(Set.of(subscriber));
        
        when(subscriptions.getSubscribers(topic)).thenReturn(subscribers);
        
        payload.put("value", "payload");
        messageQueue.add(message);
        
        intercommunicationModule.start();
        Thread.sleep(1000);
        intercommunicationModule.interrupt();
        
        verify(subscriber).acceptMessage(message);
        assertTrue(messageQueue.isEmpty());
    }
    
    @Test
    public void testRunException() throws InterruptedException, UnregisteredTopicException {
        String topic = "/test";
        JSONObject payload = new JSONObject();
        Priority priority = Priority.NORMAL;
        Message message = new Message(topic, payload, priority);
        
        when(subscriptions.getSubscribers(topic)).thenThrow(UnregisteredTopicException.class);
        
        payload.put("value", "payload");
        messageQueue.add(message);
        
        intercommunicationModule.start();
        Thread.sleep(1000);
        intercommunicationModule.interrupt();
        
        verify(subscriber, times(0)).acceptMessage(message);
        assertTrue(messageQueue.isEmpty());
    }
    
}
