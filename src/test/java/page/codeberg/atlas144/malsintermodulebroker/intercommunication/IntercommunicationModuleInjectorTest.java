// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.intercommunication;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class IntercommunicationModuleInjectorTest {
    
    @Test
    public void testInitialization() {
        IntercommunicationModuleInjector intercommunicationModuleInjector = new IntercommunicationModuleInjector();
        
        assertNotNull(intercommunicationModuleInjector);
    }
    
    @Test
    public void testGetIntercommunicationModule() {
        IntercommunicationModuleInjector.getIntercommunicationModule();
    }
    
}
