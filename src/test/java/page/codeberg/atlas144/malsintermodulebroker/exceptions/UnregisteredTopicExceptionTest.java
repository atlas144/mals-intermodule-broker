// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class UnregisteredTopicExceptionTest {
    
    @Test
    public void testInitialization() {
        UnregisteredTopicException unregisteredTopicException = new UnregisteredTopicException("test");
        
        assertNotNull(unregisteredTopicException);
    }
    
    @Test
    public void testCustomMessage() {
        String topic = "/test";
        String message = String.format("Topic '%s' in not registered topic", topic);
        UnregisteredTopicException unregisteredTopicException = new UnregisteredTopicException(topic);
        
        assertEquals(message, unregisteredTopicException.getMessage());
    }
    
    @Test
    public void testGetTopic() {
        String topic = "/test";
        UnregisteredTopicException unregisteredTopicException = new UnregisteredTopicException(topic);
        
        assertEquals(topic, unregisteredTopicException.getTopic());
    }
    
}
