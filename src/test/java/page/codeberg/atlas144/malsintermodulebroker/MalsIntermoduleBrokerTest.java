// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
import page.codeberg.atlas144.malsintermodulebroker.intercommunication.IntercommunicationModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
@RunWith(MockitoJUnitRunner.class)
public class MalsIntermoduleBrokerTest {
    
    private static final String MODULE_NAME = "testModule";
    private static final String TOPIC = "topic";
    private static final List<String> TOPICS = List.of(TOPIC);
    private static final String DEFAULT_PAYLOAD_KEY = "value";
    private static final String STRING_PAYLOAD = "payload";
    private static final double NUMERIC_PAYLOAD = 20;
    private static final boolean BOOLEAN_PAYLOAD = true;
    private static final Priority PRIORITY = Priority.NORMAL;
    
    private ConcurrentMap<String, TaskModule> taskModules;
    private MalsIntermoduleBroker malsIntermoduleBroker;
    @Mock
    private IntercommunicationModule intercommunicationModule;
    @Mock
    private TaskModule taskModule;
    
    @Before
    public void setup() {
        taskModules = new ConcurrentHashMap<>();
        malsIntermoduleBroker = new MalsIntermoduleBroker(taskModules, intercommunicationModule);
    }
    
    @Test
    public void registerModuleAddsGivenModuleToModulesList() {
        when(taskModule.getModuleName()).thenReturn(MODULE_NAME);
        
        malsIntermoduleBroker.registerModule(taskModule);
        
        assertEquals(taskModule, taskModules.get(MODULE_NAME));
    }
    
    public class JsonMatcher implements ArgumentMatcher<JSONObject> {
        private final JSONObject left;

        public JsonMatcher(JSONObject left) {
            this.left = left;
        }

        @Override
        public boolean matches(JSONObject right) {
            return left.similar(right);
        }
    }
    
    @Test
    public void publishWithStringPayloadCallsIntercommunicationPublishWithGivenPayload() {
        malsIntermoduleBroker.publish(TOPIC, STRING_PAYLOAD, PRIORITY);
        
        JSONObject payloadJson = new JSONObject();
        
        payloadJson.put(DEFAULT_PAYLOAD_KEY, STRING_PAYLOAD);
        
        verify(intercommunicationModule).publish(eq(TOPIC), argThat(new JsonMatcher(payloadJson)), eq(PRIORITY));
    }
    
    @Test
    public void publishWithNumericPayloadCallsIntercommunicationPublishWithGivenPayload() {
        malsIntermoduleBroker.publish(TOPIC, NUMERIC_PAYLOAD, PRIORITY);
        
        JSONObject payloadJson = new JSONObject();
        
        payloadJson.put(DEFAULT_PAYLOAD_KEY, NUMERIC_PAYLOAD);
        
        verify(intercommunicationModule).publish(eq(TOPIC), argThat(new JsonMatcher(payloadJson)), eq(PRIORITY));
    }
    
    @Test
    public void publishWithBooleanPayloadCallsIntercommunicationPublishWithGivenPayload() {
        malsIntermoduleBroker.publish(TOPIC, BOOLEAN_PAYLOAD, PRIORITY);
        
        JSONObject payloadJson = new JSONObject();
        
        payloadJson.put(DEFAULT_PAYLOAD_KEY, BOOLEAN_PAYLOAD);
        
        verify(intercommunicationModule).publish(eq(TOPIC), argThat(new JsonMatcher(payloadJson)), eq(PRIORITY));
    }
    
    @Test
    public void publishWithCustomPayloadCallsIntercommunicationPublishWithGivenPayload() {
        JSONObject payloadJson = new JSONObject();
        
        payloadJson.put("test1", STRING_PAYLOAD);
        payloadJson.put("test2", NUMERIC_PAYLOAD);
        
        malsIntermoduleBroker.publish(TOPIC, payloadJson, PRIORITY);
        
        verify(intercommunicationModule).publish(eq(TOPIC), argThat(new JsonMatcher(payloadJson)), eq(PRIORITY));
    }
    
    @Test
    public void publishWithoutPayloadCallsIntercommunicationPublishWithEmptyPayload() {
        malsIntermoduleBroker.publish(TOPIC, PRIORITY);
        
        verify(intercommunicationModule).publish(eq(TOPIC), argThat(new JsonMatcher(new JSONObject())), eq(PRIORITY));
    }
    
    @Test
    public void subscribeWithSingleTopicCallsIntercommunicationSubscribe() {
        malsIntermoduleBroker.subscribe(TOPIC, taskModule);
        
        verify(intercommunicationModule).subscribe(TOPIC, taskModule);
    }
    
    @Test
    public void subscribeWithMultipleTopicCallsIntercommunicationSubscribe() {
        malsIntermoduleBroker.subscribe(TOPICS, taskModule);
        
        verify(intercommunicationModule).subscribe(TOPICS, taskModule);
    }
    
    @Test
    public void unsubscribeCallsIntercommunicationUnsubscribe() {
        malsIntermoduleBroker.unsubscribe(TOPIC, taskModule);
        
        verify(intercommunicationModule).unsubscribe(TOPIC, taskModule);
    }
    
    @Test
    public void startCallsStartOnIntercommunicationModuleAndAllRegisteredTaskModules() {
        taskModules.put(MODULE_NAME, taskModule);
        
        malsIntermoduleBroker.start();
        
        verify(intercommunicationModule).start();
        verify(taskModule).start();
    }
    
}
