// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.mock;
import org.mockito.junit.MockitoJUnitRunner;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.exceptions.UnregisteredTopicException;

/**
 *
 * @author atlas144
 */
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionsTrieTest {
    
    private SubscriptionsTrie subscriptionsTrie;
    private SubscriptionsTrieNode<TaskModule> rootNode;
    private ReadWriteLock lock;
    private List<TaskModule> subscribers;
    
    @Before
    public void setup() {
        rootNode = SubscriptionsTrieNodeInjector.getSubscriptionsTrieNode();
        lock = new ReentrantReadWriteLock();
        
        subscriptionsTrie = new SubscriptionsTrie(rootNode, lock);
        
        subscribers = new ArrayList<>(1);
        subscribers.add(mock(TaskModule.class));
    }
    
    private void setupTrie() {
        for (int i = 0; i < 10; i++) {
            subscribers.add(mock(TaskModule.class));
        }
        
        subscriptionsTrie.subscribe("my", subscribers.get(0));
        subscriptionsTrie.subscribe("my", subscribers.get(1));
        subscriptionsTrie.subscribe("my/test1", subscribers.get(0));
        subscriptionsTrie.subscribe("my/test1", subscribers.get(2));
        subscriptionsTrie.subscribe("my/test1", subscribers.get(3));
        subscriptionsTrie.subscribe("my/test2/topic3/a", subscribers.get(0));
        subscriptionsTrie.subscribe("my/test2/topic3/a", subscribers.get(4));
        subscriptionsTrie.subscribe("my/test3", subscribers.get(5));
        subscriptionsTrie.subscribe("my/test1/topic1", subscribers.get(0));
        subscriptionsTrie.subscribe("my/test1/topic1", subscribers.get(6));
        subscriptionsTrie.subscribe("my/test1/topic1/a", subscribers.get(0));
        subscriptionsTrie.subscribe("my/test1/topic1/a", subscribers.get(7));
        subscriptionsTrie.subscribe("my/test1/topic1/b", subscribers.get(8));
        subscriptionsTrie.subscribe("my/test1/topic2", subscribers.get(9));
        subscriptionsTrie.subscribe("my/test1/topic2/a", subscribers.get(10));
    }
    
    @Test
    public void testSubscribe() {
        String topic1 = "test1";
        String topic2 = "test2";
        String topic3 = "test3";
        String topic = String.format("%s/%s/%s", topic1, topic2, topic3);
        
        subscriptionsTrie.subscribe(topic, subscribers.get(0));
                        
        SubscriptionsTrieNode node1 = rootNode.getChildren().get(topic1);
        SubscriptionsTrieNode node2 = (SubscriptionsTrieNode) node1.getChildren().get(topic2);
        SubscriptionsTrieNode node3 = (SubscriptionsTrieNode) node2.getChildren().get(topic3);
        
        assertTrue(rootNode.getChildren().containsKey(topic1));
        assertTrue(node1.getChildren().containsKey(topic2));
        assertTrue(node2.getChildren().containsKey(topic3));
        assertTrue(node3.getSubscribers().contains(subscribers.get(0)));
    }
    
    @Test
    public void testSubscribePrefixed() {
        String topic1 = "test1";
        String topic2 = "test2";
        String topic3 = "test3";
        String topic = String.format("/%s/%s/%s", topic1, topic2, topic3);
        
        subscriptionsTrie.subscribe(topic, subscribers.get(0));
                        
        SubscriptionsTrieNode node1 = rootNode.getChildren().get(topic1);
        SubscriptionsTrieNode node2 = (SubscriptionsTrieNode) node1.getChildren().get(topic2);
        SubscriptionsTrieNode node3 = (SubscriptionsTrieNode) node2.getChildren().get(topic3);
        
        assertTrue(rootNode.getChildren().containsKey(topic1));
        assertTrue(node1.getChildren().containsKey(topic2));
        assertTrue(node2.getChildren().containsKey(topic3));
        assertTrue(node3.getSubscribers().contains(subscribers.get(0)));
    }
    
    /*@Test
    public void testUnsubscribe() throws UnregisteredTopicException {
        String topic = "test";
        SubscriptionsTrieNode testNode = SubscriptionsTrieNodeInjector.getSubscriptionsTrieNode();
        
        testNode.addSubscriber(subscriber1);
        rootNode.getChildren().put(topic, testNode);
        
        //subscriptionsTrie.unsubscribe(topic, subscriber1);
        
        //assertEquals(rootNode.getChildren().size(), 0);
    }
    
    @Test(expected = UnregisteredTopicException.class)
    public void testUnsubscribeException() throws UnregisteredTopicException {
        String topic = "/test";
        
        subscriptionsTrie.unsubscribe(topic, subscribers.get(0));
    }*/
    
    @Test
    public void getSubscribersReturnsExpectedResultForFullTopicMatcher() throws UnregisteredTopicException {
        setupTrie();
        
        String topic = "my/test1/topic1/a";
        
        Set result = subscriptionsTrie.getSubscribers(topic);
        Set expectedResult = Set.of(subscribers.get(0), subscribers.get(7));
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void getSubscribersReturnsExpectedResultForPartialTopicMatcher() throws UnregisteredTopicException {
        setupTrie();
        
        String topic = "my/test1";
        
        Set result = subscriptionsTrie.getSubscribers(topic);
        Set expectedResult = Set.of(subscribers.get(0), subscribers.get(2), subscribers.get(3));
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void getSubscribersReturnsExpectedResultForTopicMatcherWithSingleLevelWildcard() throws UnregisteredTopicException {
        setupTrie();
        
        String topic = "my/test1/+/a";
        
        Set result = subscriptionsTrie.getSubscribers(topic);
        Set expectedResult = Set.of(subscribers.get(0), subscribers.get(7), subscribers.get(10));
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void getSubscribersReturnsExpectedResultForTopicMatcherWithMultipleSingleLevelWildcard() throws UnregisteredTopicException {
        setupTrie();
        
        String topic = "my/+/+/a";
        
        Set result = subscriptionsTrie.getSubscribers(topic);
        Set expectedResult = Set.of(subscribers.get(0), subscribers.get(4), subscribers.get(7), subscribers.get(10));
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void getSubscribersReturnsExpectedResultForTopicMatcherWithMultiLevelWildcard() throws UnregisteredTopicException {
        setupTrie();
        
        String topic = "my/test1/#";
        
        Set result = subscriptionsTrie.getSubscribers(topic);
        Set expectedResult = Set.of(subscribers.get(0), subscribers.get(6), subscribers.get(7), subscribers.get(8), subscribers.get(9), subscribers.get(10));
        
        assertEquals(expectedResult, result);
    }
    
    @Test(expected = UnregisteredTopicException.class)
    public void getSubscribersThrowsWhenTopicIsNotRegistered() throws UnregisteredTopicException {
        String topic = "notRegistered";
        
        subscriptionsTrie.getSubscribers(topic);
    }
    
    @Test
    public void testIsEmpty() {
        assertTrue(subscriptionsTrie.isEmpty());
    }
    
}
