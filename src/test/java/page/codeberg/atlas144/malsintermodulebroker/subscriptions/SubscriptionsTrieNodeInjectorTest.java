// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class SubscriptionsTrieNodeInjectorTest {
    
    @Test
    public void testInitialization() {
        SubscriptionsTrieNodeInjector subscriptionsTrieNodeInjector = new SubscriptionsTrieNodeInjector();
        
        assertNotNull(subscriptionsTrieNodeInjector);
    }
    
    @Test
    public void testGetSubscriptionsTrieNode() {
        SubscriptionsTrieNodeInjector.getSubscriptionsTrieNode();
    }
    
}
