// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
@RunWith(MockitoJUnitRunner.class)
public class TaskModuleCoreTest {
    
    private static final String MODULE_NAME = "test";
    private static final String ANOTHER_MODULE_NAME = "another_test";
    private Message message;
    private TaskModuleCore taskModule;
    private AtomicBoolean runningState;
    private BlockingQueue<Message> messageQueue;
    @Mock
    private MalsIntermoduleBroker broker;
    
    @Before
    public void setup() {
        message = new Message("topic", new JSONObject(), Priority.NORMAL);
        runningState = new AtomicBoolean();
        messageQueue = new PriorityBlockingQueue<>();
        
        taskModule = new DummyTaskModule(
            MODULE_NAME,
            messageQueue,
            runningState
        );
        
        taskModule.registerBroker(broker);
    }
    
    @Test
    public void getModuleNameReturnsNameCorrectly() {
        assertEquals(MODULE_NAME, taskModule.getModuleName());
    }
    
    @Test
    public void acceptMessageAddsGivenMessageToQueue() throws InterruptedException {
        taskModule.acceptMessage(message);
        
        assertTrue(messageQueue.contains(message));
    }
    
    @Test
    public void killSetsRunningFalse() {
        runningState.set(true);
        
        taskModule.kill();
        
        assertTrue(!runningState.get());
    }
    
    @Test
    public void hashCodeMatchModuleWithSameName() {
        assertEquals(59 * 5 + Objects.hashCode(MODULE_NAME), taskModule.hashCode());
    }
    
    @Test
    public void hashCodeNotMatchModeuleWithDifferentName() {
        assertNotEquals(59 * 5 + Objects.hashCode(ANOTHER_MODULE_NAME), taskModule.hashCode());
    }
    
    @Test
    public void equalsReturnsTrueForSameModule() {
        assertTrue(taskModule.equals(taskModule));
    }
    
    @Test
    public void equalsReturnsTrueForEqualModule() {
        TaskModuleCore referenceTaskModule = new DummyTaskModule(
                MODULE_NAME, 
                new PriorityBlockingQueue<>(), 
                new AtomicBoolean()
        );
        
        assertTrue(taskModule.equals(referenceTaskModule));
    }
    
    @Test
    public void equalsReturnsFalseForDifferentNameModule() {
        TaskModuleCore referenceTaskModule = new DummyTaskModule(
                ANOTHER_MODULE_NAME, 
                new PriorityBlockingQueue<>(), 
                new AtomicBoolean()
        );
        
        assertTrue(!taskModule.equals(referenceTaskModule));
    }
    
    @Test
    public void equalsReturnsFalseForNull() {
        TaskModuleCore referenceTaskModule = null;
        
        assertTrue(!taskModule.equals(referenceTaskModule));
    }
    
    private class DummyTaskModule extends TaskModuleCore {
        
        public DummyTaskModule(
            String moduleName,
            BlockingQueue<Message> messageQueue,
            AtomicBoolean runningState
        ) {
            super(
                moduleName,
                messageQueue,
                runningState
            );
        }

        @Override
        protected void setup() {}

        @Override
        protected void loop() {}
        
    }
    
}
