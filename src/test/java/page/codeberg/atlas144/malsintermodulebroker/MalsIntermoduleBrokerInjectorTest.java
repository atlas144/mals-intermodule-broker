// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class MalsIntermoduleBrokerInjectorTest {
    
    @Test
    public void getMalsIntermoduleBrokerReturnsValidBroker() {
        assertNotNull(MalsIntermoduleBrokerInjector.getMalsIntermoduleBroker());
    }
    
}
