// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.intercommunication;

import java.util.concurrent.PriorityBlockingQueue;
import page.codeberg.atlas144.malsintermodulebroker.subscriptions.SubscriptionsTrieInjector;

/**
 *
 * @author atlas144
 */
public class IntercommunicationModuleInjector {
    
    public static IntercommunicationModule getIntercommunicationModule() {
        return new IntercommunicationModule(
            new PriorityBlockingQueue<>(),
            SubscriptionsTrieInjector.getSubscriptionsTrie()
        );
    }
    
}
