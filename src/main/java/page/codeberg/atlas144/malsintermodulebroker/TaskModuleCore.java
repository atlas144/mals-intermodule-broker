// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;

/**
 * Template for subroutines representing individual modules of the system.
 * 
 * User extends the template and adds the code for the subsystem activity 
 * to the <code>setup</code> and <code>loop</code> methods.
 *
 * @author atlas144
 */
public abstract class TaskModuleCore extends Thread {
    
    private final Logger logger;
    private final String moduleName;
    private final AtomicBoolean runningState;
    protected final BlockingQueue<Message> messageQueue;
    protected MalsIntermoduleBroker broker;
    
    /**
     * Here should be placed the code that should is executed only once,
     * at the beginning of the module action.
     * @throws java.lang.InterruptedException the method may take messages from
     * the queue (and during the waiting for the message it may be interrupted)
     */
    protected abstract void setup() throws InterruptedException;
    
    /**
     * Here should be placed the code that is executed in a loop for the whole
     * running time of the module.
     * @throws java.lang.InterruptedException the method may take messages from
     * the queue (and during the waiting for the message it may be interrupted)
     */
    protected abstract void loop() throws InterruptedException;
        
    /**
     * Creates instance of the task module.
     * 
     * @param moduleName name of the module (must be unique)
     * @param messageQueue queue for messages accepted from broker
     * @param runningState indicator of running state of module
     */
    public TaskModuleCore(String moduleName, BlockingQueue<Message> messageQueue, AtomicBoolean runningState) {
        logger = LoggerFactory.getLogger(TaskModuleCore.class);
        this.moduleName = moduleName;
        this.runningState = runningState;
        this.messageQueue = messageQueue;
        broker = null;
        
        this.runningState.set(true);
        
        logger.trace("Task module '{}' successfully created", moduleName);
    }

    /**
     * Returns name of the task module.
     * 
     * @return module name
     */
    public String getModuleName() {
        return moduleName;
    }
    
    /**
     * Inserts message to the message queue of the task module.
     * 
     * @param message message to be inserted
     */
    public void acceptMessage(Message message) {
        messageQueue.add(message);
        logger.trace("Message successfully accepted: {}", message);
    }
    
    public void registerBroker(MalsIntermoduleBroker broker) {
        this.broker = broker;
        logger.debug("Broker registered successfully");
    }
    
    public void kill() {
        runningState.set(false);
        logger.debug("Module killed successfully");
    }

    /**
     * Counts hash code from module name.
     * 
     * @return hash code of the module
     */
    @Override
    public int hashCode() {
        return 59 * 5 + Objects.hashCode(this.moduleName);
    }
    
    /**
     * Compares names of both modules. If they are same, both modules are same.
     * 
     * @param object module to be compared
     * @return <code>true</code> if both module names are equal
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
         
        TaskModuleCore module = (TaskModuleCore) object;
        
        return moduleName.equals(module.getModuleName());
    }
    
    @Override
    public void run() {
        try {
            setup();
            
            while (runningState.get()) {
                loop();
            }
        } catch (InterruptedException ex) {
            logger.debug("Stopping module");
        } finally {
            logger.debug("Module stopped successfully");            
        }
    }
    
}