// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.conversion;

import java.util.regex.Pattern;

/**
 *
 * @author atlas144
 */
public class PayloadConvertor {
    
    public static final String DELIMITER = "|";
    
    public static String encodePayload(short[] payload) {
        StringBuilder builder = new StringBuilder();
        
        for (int i = 0; i < payload.length; ++i) {
            builder.append(payload[i]);
            if (i < (payload.length - 1)) builder.append(DELIMITER);
        }
        
        return builder.toString();
    }
    
    public static short[] decodePayload(String payload) {
        String[] stringPayload = payload.split(Pattern.quote(DELIMITER));
        short[] shortPayload = new short[stringPayload.length];
        
        for (int i = 0; i < stringPayload.length; ++i) {
            shortPayload[i] = Short.parseShort(stringPayload[i]);
        }
        
        return shortPayload;
    }
    
}
