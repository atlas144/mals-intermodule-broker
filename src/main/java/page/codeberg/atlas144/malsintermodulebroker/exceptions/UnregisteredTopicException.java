// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.exceptions;

/**
 * An exception that means the present topic is not registered (no subscribers
 * belong to it).
 * @author atlas144
 */
public class UnregisteredTopicException extends Exception {
    
    private final String topic;
    
    public UnregisteredTopicException(String topic) {
        super(String.format("Topic '%s' in not registered topic", topic));
        
        this.topic = topic;
    }

    /**
     * Returns topic to which the exception refers.
     * @return topic to which the exception refers
     */
    public String getTopic() {
        return topic;
    }

}
