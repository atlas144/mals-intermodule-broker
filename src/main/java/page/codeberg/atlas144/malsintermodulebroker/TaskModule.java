package page.codeberg.atlas144.malsintermodulebroker;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author atlas144
 */
public abstract class TaskModule extends TaskModuleCore {

    public TaskModule(String moduleName) {
        super(moduleName, new PriorityBlockingQueue(), new AtomicBoolean());
    }
    
}
