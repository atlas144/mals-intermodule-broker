// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.model;

import org.json.JSONObject;

/**
 * Message object used to transfer information between platform components.
 *
 * @author atlas144
 */
public record Message(String topic, JSONObject payload, Priority priority) implements Comparable {
    /**
     * Compares both messages by priority.
     * 
     * @param object message to be compared
     * @return if <i>this priority</i> is greater than the other one, returns 
     * <code>1</code>. If it is smaller, returns <code>-1</code> and if they are
     * equal, returns <code>0</code>.
     */
    @Override
    public int compareTo(Object object) {
        byte thisPriorityValue = priority.getValue();
        byte otherPriorityValue = ((Message) object).priority().getValue();
        
        if (thisPriorityValue > otherPriorityValue) {
            return 1;
        } else if (thisPriorityValue < otherPriorityValue) {
            return -1;
        } else {
            return 0;
        }
    }    
}
