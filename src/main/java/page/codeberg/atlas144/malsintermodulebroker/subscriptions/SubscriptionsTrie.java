// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import page.codeberg.atlas144.malsintermodulebroker.exceptions.UnregisteredTopicException;

/**
 * A data structure representing a tree of subscriptions, their topics and 
 * subscribers.
 * @author atlas144
 * @param <Subscriber> data type of the subscribers
 */
public class SubscriptionsTrie<Subscriber> {
    private static final String LEVEL_SEPARATOR = "/";
    private static final String SINGLE_LEVEL_WILDCARD = "+";
    private static final String MULTI_LEVEL_WILDCARD = "#";
    
    private final SubscriptionsTrieNode<Subscriber> root;
    private final Lock readLock;
    private final Lock writeLock;
    
    private List<String> parseTopic(String topic) {
        return new LinkedList<>(List.of((topic.startsWith(LEVEL_SEPARATOR) ? topic.substring(1) : topic).split(LEVEL_SEPARATOR)));
    }
    
    /*private List<SubscriptionsTrieNode<Subscriber>> getSearchedPath(SubscriptionsTrieNode startingNode, String[] topicLevels) {
        List<SubscriptionsTrieNode<Subscriber>> searchedPath = new LinkedList<>();

        try {
            readLock.lock();
            
            SubscriptionsTrieNode<Subscriber> currentNode = startingNode;
            
            searchedPath.add(startingNode);
            
            for (String topicLevel : topicLevels) {
                SubscriptionsTrieNode node = currentNode.getChildren().get(topicLevel);

                searchedPath.add(node);
                currentNode = node;
            }
        } finally {
            readLock.unlock();
        }
        
        return searchedPath;
    }*/

    /**
     * Creates new trie.
     * @param rootNode root node of the trie
     * @param lock RW lock of the trie
     */
    public SubscriptionsTrie(
        SubscriptionsTrieNode<Subscriber> rootNode,
        ReadWriteLock lock
    ) {
        this.root = rootNode;
        readLock = lock.readLock();
        writeLock = lock.writeLock();
    }

    /**
     * Creates a new subscription.
     * @param topic topic of the subscription
     * @param subscriber subject of the subscription
     */
    public void subscribe(String topic, Subscriber subscriber) {
        try {
            writeLock.lock();
            
            SubscriptionsTrieNode<Subscriber> currentNode = root;

            for (String topicLevel : parseTopic(topic)) {
                currentNode = currentNode.getChildren().computeIfAbsent(topicLevel, (node -> SubscriptionsTrieNodeInjector.getSubscriptionsTrieNode()));
            }

            if (!currentNode.getSubscribers().contains(subscriber)) {
                currentNode.addSubscriber(subscriber);
            }
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Ends an subscription.
     * @param topic topic of the subscription
     * @param subscriber subject of the subscription
     * @throws UnregisteredTopicException if the topic to be unsubscribed is not yet subscribed
     */
    public void unsubscribe(String topic, Subscriber subscriber) throws UnregisteredTopicException {
        throw new UnsupportedOperationException("Unsubscribe is not supported yet!");
        
        /*String[] topicLevels = parseTopic(topic);
        
        try {
            writeLock.lock();
            
            LinkedList<SubscriptionsTrieNode<Subscriber>> searchedPath = (LinkedList<SubscriptionsTrieNode<Subscriber>>) getSearchedPath(root, topicLevels);

            if (searchedPath.peekLast() != null) {
                List<Subscriber> subscribers = searchedPath.peekLast().getSubscribers();

                subscribers.remove(subscriber);

                for (int i = searchedPath.size() - 1; i > 0; i++) {
                    SubscriptionsTrieNode<Subscriber> leafNode = searchedPath.pollLast();

                    if (leafNode.getChildren().isEmpty() && leafNode.getSubscribers().isEmpty()) {
                        searchedPath.peekLast().getChildren().remove(topicLevels[i - 1]);
                    } else {
                        break;
                    }
                }
            } else {
                throw new UnregisteredTopicException(topic);
            }
        } finally {
            writeLock.unlock();
        }*/
    }

    /**
     * Returns list of subscribers of the topic.
     * @param topic topic of the subscription
     * @return list of subscribers of the topic
     * @throws UnregisteredTopicException if the given topic is not yet subscribed
     */
    public Set<Subscriber> getSubscribers(String topic) throws UnregisteredTopicException {
        List topicLevels = parseTopic(topic);
        
        try {
            readLock.lock();
        
            Set subscribers = processNode(root, topicLevels);
            
            if (subscribers.isEmpty()) {
                throw new UnregisteredTopicException(topic);
            }
            
            return subscribers;
        } finally {
            readLock.unlock();
        }
    }
    
    private Set<Subscriber> processNode(SubscriptionsTrieNode node, List<String> remainingLevels) {
        if (remainingLevels.isEmpty()) {
            return node.getSubscribers();
        }
        
        String currentLevel = remainingLevels.remove(0);
        Map<String, SubscriptionsTrieNode> childNodes = node.getChildren();
        Set<Subscriber> subscribers = new HashSet<>();
        
        switch (currentLevel) {
            case SINGLE_LEVEL_WILDCARD:
                for (SubscriptionsTrieNode childNode : childNodes.values()) {
                    subscribers.addAll(processNode(childNode, new ArrayList<>(remainingLevels)));
                }
                
                break;
            case MULTI_LEVEL_WILDCARD:
                for (SubscriptionsTrieNode childNode : childNodes.values()) {
                    subscribers.addAll(processWildcardNode(childNode));
                }
                
                break;
            default:
                SubscriptionsTrieNode child = childNodes.get(currentLevel);
                
                if (null != child) {
                    subscribers.addAll(processNode(child, remainingLevels));
                }
        }
        
        return subscribers;
    }
    
    private Set<Subscriber> processWildcardNode(SubscriptionsTrieNode node) {
        Map<String, SubscriptionsTrieNode> childNodes = node.getChildren();
        Set<Subscriber> subscribers = node.getSubscribers();
        
        for (SubscriptionsTrieNode childNode : childNodes.values()) {
            subscribers.addAll(processWildcardNode(childNode));
        }
        
        return subscribers;
    }

    /**
     * Checks if there are any subscriptions.
     * @return true if there are any subscriptions, else false
     */
    public boolean isEmpty() {
        try {
            readLock.lock();
            
            return root.getChildren().isEmpty();
        } finally {
            readLock.unlock();
        }
    }
}
