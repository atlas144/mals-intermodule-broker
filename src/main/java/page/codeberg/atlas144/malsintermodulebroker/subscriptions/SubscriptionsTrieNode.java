// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.Map;
import java.util.Set;

/**
 * Single node in subscription trie data structure.
 * @author atlas144
 * @param <Subscriber> data type of the subscribers
 */
public class SubscriptionsTrieNode<Subscriber> {
    
    private final Map<String, SubscriptionsTrieNode> children;
    private final Set<Subscriber> subscribers;
    
    /**
     * Creates node.
     * @param children map of children nodes
     * @param subscribers set of subscribed modules
     */
    public SubscriptionsTrieNode(
        Map<String, SubscriptionsTrieNode> children,
        Set<Subscriber> subscribers
    ) {
        this.children = children;
        this.subscribers = subscribers;
    }

    /**
     * Returns map of nodes child-nodes.
     * @return map of nodes child-nodes
     */
    public Map<String, SubscriptionsTrieNode> getChildren() {
        return children;
    }

    /**
     * Returns set of nodes subscribers.
     * @return set of nodes subscribers
     */
    public Set<Subscriber> getSubscribers() {
        return subscribers;
    }

    /**
     * Adds new subscriber to the list of nodes subscribers.
     * @param subscriber new subscriber to be added
     */
    public void addSubscriber(Subscriber subscriber) {
        this.subscribers.add(subscriber);
    }

    /**
     * Removes given subscriber from nodes subscribers list.
     * @param subscriber subscriber to be removed
     */
    public void removeSubscriber(Subscriber subscriber) {
        this.subscribers.remove(subscriber);
    }
    
}