// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author atlas144
 */
public class SubscriptionsTrieInjector {
    
    public static SubscriptionsTrie getSubscriptionsTrie() {
        return new SubscriptionsTrie(
            SubscriptionsTrieNodeInjector.getSubscriptionsTrieNode(),
            new ReentrantReadWriteLock()
        );
    }
    
}
